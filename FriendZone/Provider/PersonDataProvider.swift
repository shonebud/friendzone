//
//  PersonDataProvider.swift
//  FriendZone
//
//  Created by Vitalii Todorovych on 2/14/17.
//  Copyright © 2017 Vitalii Todorovych. All rights reserved.
//

import Foundation
import CoreLocation

let kParamsErrorCode = 111
let kNoLocationErrorCode = 112

protocol PersonDataProviderProtecol {
    func getMyLocation(doneBlock: (result: CLLocationCoordinate2D?, error: NSError?) -> Void)
    func getFriendLocation(myPhone : String!, friendPhone : String!, myLocationLatitude : String!, myLocationLongitude : String!, doneBlock: (result: CLLocationCoordinate2D?, error: NSError?) -> Void)
}

class PersonDataProvider : NSObject, PersonDataProviderProtecol, CLLocationManagerDelegate {
    
    var getLocationDoneBlock: ((result: CLLocationCoordinate2D?, error: NSError?) -> Void)?
    
    lazy var locationManager : CLLocationManager = {
        var tempLocationManager = CLLocationManager()
        
        // Ask for Authorisation from the User.
        tempLocationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            tempLocationManager.delegate = self
            tempLocationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        return tempLocationManager
    }()
    
    lazy var httpProvider : HTTPProvider = {
        var tempProvider = HTTPProvider()
        return tempProvider
    }()
    
    // Mark : PersonDataProviderProtecol
    
    func getMyLocation(doneBlock: (result: CLLocationCoordinate2D?, error: NSError?) -> Void) {
        self.getLocationDoneBlock = doneBlock
        self.locationManager.startUpdatingLocation()
    }
    
    func getFriendLocation(myPhone : String!, friendPhone : String!, myLocationLatitude : String!, myLocationLongitude : String!, doneBlock: (result: CLLocationCoordinate2D?, error: NSError?) -> Void) {
        if let url = NSURL(string: "https://whereu.enkonix.com/api/range/") {
            
            let params : Dictionary<String, String>! = ["my_phone":myPhone,"friend_phone":friendPhone,"lat":myLocationLatitude,"long":myLocationLongitude]
            
            self.httpProvider.getData(url, params: params, doneBlock: { (result, error) in
                
                if let result = result, errors = result["errors"] {
                    doneBlock(result:nil, error:NSError(domain: "", code: kParamsErrorCode, userInfo: ["description" : errors]))
                }
                    
                if let result = result, friendLat = result["friend_lat"], friendLong = result["friend_long"] {
                    
                    if let friendLatValue = friendLat.doubleValue, friendLongValue = friendLong.doubleValue{
                        let locationCoordinate = CLLocationCoordinate2DMake(friendLatValue, friendLongValue)
                        doneBlock(result:locationCoordinate, error: nil)
                    }else{
                        doneBlock(result:nil, error:NSError(domain: "", code: kNoLocationErrorCode, userInfo: nil))
                    }
                }else if let error = error{
                    doneBlock(result:nil, error:error)
                }
                
            })
        }
    }
    
    
    // Mark : CLLocationManagerDelegate
    
    @objc func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = (manager.location?.coordinate)!
        
        if let doneBlock = self.getLocationDoneBlock {
            doneBlock(result: locValue, error: nil)
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    @objc func locationManager(manager: CLLocationManager, didFinishDeferredUpdatesWithError error: NSError?) {
        
        if let doneBlock = self.getLocationDoneBlock {
            doneBlock(result: nil, error: error)
        }
    }
}