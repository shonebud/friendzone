//
//  HTTPProvider.swift
//  FriendZone
//
//  Created by Vitalii Todorovych on 2/14/17.
//  Copyright © 2017 Vitalii Todorovych. All rights reserved.
//

import Foundation

protocol HTTPProviderProtocol {
    func getData(url : NSURL!, params : Dictionary<String, String>!, doneBlock: (result: NSDictionary?, error: NSError?) -> Void)
}

class HTTPProvider: HTTPProviderProtocol {

    var doneBlock : ((result: NSDictionary?, error: NSError?) -> Void)?

    let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    var dataTask: NSURLSessionDataTask?

    func getData(url : NSURL!, params : Dictionary<String, String>!, doneBlock: (result: NSDictionary?, error: NSError?) -> Void) {
    
        self.doneBlock = doneBlock
        
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let request = NSMutableURLRequest(URL:url)
        request.HTTPMethod = "POST"
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(params, options:[])
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.HTTPBody = data
            
            self.dataTask = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    
                    if let doneBlock = self.doneBlock {
                        doneBlock(result: nil, error: error)
                    }
                    
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                do {
                    let json : NSDictionary?
                    
                    json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions()) as? NSDictionary
                    print(json)
                    
                    if let doneBlock = self.doneBlock {
                        doneBlock(result: json, error: nil)
                    }
                } catch {
                    print(error)
                }
                
                let responseString = String(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseString)")
                
            }
            self.dataTask!.resume()
            
        } catch {
            print("JSON serialization failed:  \(error)")
        }
    }
    
}