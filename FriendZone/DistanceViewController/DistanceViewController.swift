//
//  DistanceViewController.swift
//  FriendZone
//
//  Created by Vitalii Todorovych on 2/14/17.
//  Copyright © 2017 Vitalii Todorovych. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class DistanceViewController: UIViewController {
    
    @IBOutlet var distanceLabel: UILabel!
    @IBOutlet var mapView: MKMapView!
    
    var firstLocation : CLLocationCoordinate2D!
    var seccondLocation : CLLocationCoordinate2D!
    
    convenience init?(firstLocation: CLLocationCoordinate2D!, seccondLocation: CLLocationCoordinate2D!) {
        self.init(nibName: "DistanceViewController", bundle: NSBundle.mainBundle())
        
        self.firstLocation = firstLocation
        self.seccondLocation = seccondLocation
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showDistance()
        self.setupMapView()
        self.showFriendLocation()
    }
    
    func setupMapView() {
        let distance = self.calculateDistance()
        
        //Set MapView region
        let center = CLLocationCoordinate2D(latitude: self.seccondLocation.latitude, longitude: self.seccondLocation.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: distance / 100, longitudeDelta: distance / 100))
        self.mapView.setRegion(region, animated: true)
    }
    
    func showFriendLocation() {
        //Set Friend Annotation
        let annotation = MKPointAnnotation()
        annotation.title = "Your friend is here!"
        annotation.coordinate = self.seccondLocation
        mapView.addAnnotation(annotation)
    }
    
    func showDistance() {
        let distance = self.calculateDistance()
        self.distanceLabel.text = String(distance) + " km"
    }
    
    func calculateDistance() -> CLLocationDistance {
        let myLocation = CLLocation(latitude: firstLocation.latitude, longitude: firstLocation.longitude)
        let friendLocation = CLLocation(latitude: seccondLocation.latitude, longitude: seccondLocation.longitude)
        myLocation.distanceFromLocation(friendLocation);
        return myLocation.distanceFromLocation(friendLocation) / 1000
    }
    
    // Mark : Actions
    
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}