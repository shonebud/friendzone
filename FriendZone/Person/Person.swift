//
//  Person.swift
//  FriendZone
//
//  Created by Vitalii Todorovych on 2/14/17.
//  Copyright © 2017 Vitalii Todorovych. All rights reserved.
//

import Foundation
import CoreLocation

struct Person {
    let phoneNumber : String
    let location    : CLLocationCoordinate2D
}