//
//  ViewController.swift
//  FriendZone
//
//  Created by Vitalii Todorovych on 2/14/17.
//  Copyright © 2017 Vitalii Todorovych. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet var myPhoneNemberTextField     : UITextField!
    @IBOutlet var myLongitudeTextField       : UITextField!
    @IBOutlet var myLatitudeTextField        : UITextField!
    @IBOutlet var friendPhoneNumberTextField : UITextField!
    
    let personDataProvier : PersonDataProvider = PersonDataProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.personDataProvier.getMyLocation { (result, error) in
            
            if let locationLongitudeText = self.myLongitudeTextField.text, locationLatitudeText = self.myLatitudeTextField.text {
                if locationLongitudeText.characters.count > 0 || locationLatitudeText.characters.count > 0{
                    return
                }
            }
            
            if let result = result {
                self.myLongitudeTextField.text = String(result.longitude)
                self.myLatitudeTextField.text = String(result.latitude)
            }else if let error = error {
                print(error.description)
            }
        }
        
    }
    
    func showErrorAlert(message : String) {
        let alertController = UIAlertController(title: "Error", message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: Actions
    
    @IBAction func getDistanceAction(sender: AnyObject) {
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        self.personDataProvier.getFriendLocation(self.myPhoneNemberTextField.text, friendPhone: self.friendPhoneNumberTextField.text, myLocationLatitude: self.myLatitudeTextField.text!, myLocationLongitude: self.myLongitudeTextField.text!) { (result, error) in
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            if let result = result {
                
                let myLocationCoordinate = CLLocationCoordinate2DMake(Double(self.myLatitudeTextField.text!)!, Double(self.myLongitudeTextField.text!)!)
                
                let distanceViewController = DistanceViewController(firstLocation: myLocationCoordinate, seccondLocation: result);
                if let distanceViewController = distanceViewController {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.presentViewController(distanceViewController, animated: true, completion: nil)
                    })
                }
            }else if let error = error{
                print(error)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    switch error.code {
                    case kParamsErrorCode : self.showErrorAlert("Wrong input data")
                    case kNoLocationErrorCode : self.showErrorAlert("No location for friend")
                    default : self.showErrorAlert("Server Error. Please try again later")
                    }
                })
            }
            
        }
    }
    
}


